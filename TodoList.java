import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TodoList {
    public static void main(String[] args) {
        // We declared an ArrayList called taskList to
        // store a list of Task objects. This list is used to manage tasks.
        ArrayList<Task> taskList = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);

        int choice = 0;

        // We used a while (true) loop to repeatedly display a
        // menu of options and read the user's choice.
        while (true) {
            System.out.println("\nTo-Do List:");
            for (int i = 0; i <taskList.size(); i++) {
                System.out.println((i + 1) + ". " + taskList.get(i));
            }

            System.out.println("\nOptions:");
            System.out.println("1. Add a task");
            System.out.println("2. Remove a task");
            System.out.println("3. Mark a task as done");
            System.out.println("4. Quit");

            System.out.print("Enter a task: ");

            try {
                choice = scanner.nextInt();
            }
            catch (InputMismatchException e){
                System.out.println("Invalid input. Please enter a number.");
            }

            scanner.nextLine();

            switch (choice) {
                case 1:
                    System.out.print("Enter a task: ");
                    String taskDescription = scanner.nextLine();
                    taskList.add(new Task(taskDescription));
                    break;

                case 2:
                    System.out.print("Enter the task number to remove: ");
                    int taskNumber = scanner.nextInt();
                    scanner.nextLine();
                    if (taskNumber > 0 && taskNumber <= taskList.size()) {
                        taskList.remove(taskNumber - 1);
                    }
                    else {
                        System.out.println("Invalid task number: ");
                    }
                    break;

                case 3:
                    System.out.println("Enter the task number to mark as done: ");
                    int taskToMarkDone = scanner.nextInt();
                    scanner.nextLine();
                    if (taskToMarkDone > 0 && taskToMarkDone <= taskList.size()) {
                        taskList.get(taskToMarkDone - 1).markAsDone();
                    }
                    else {
                        System.out.println("Invalid task number.");
                    }
                    break;

                case 4:
                    System.out.println("Goodbye!");
                    scanner.close();
                    return;

                default:
                    System.out.println("Invalid choice. Please select 1, 2, or 3.");
                    break;
            }
        }
    }
}
