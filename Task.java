// We created a Task class that represents a task to be completed.
// It has attributes for task description and a boolean flag to
// mark whether the task is done. We also created methods to get
// the description, check if the task is done, and mark it as done.

// We utilized object-oriented programming by creating a Task class.
// Each task is represented as an object of the Task class. This allows
// us to encapsulate task data and behavior within objects, making
// it easier to manage and manipulate tasks.

// The code defines a class called Task. This class is a blueprint
// for creating objects, and each object created from this class will
// have its own set of attributes and methods.
public class Task {
    // Attributes: You are right that the Task class has two attributes:
    // description: This attribute stores the description of the task (e.g., "do the dishes").
    // isDone: This attribute is a boolean flag that indicates whether the task
    // is marked as done (completed). It's initially set to false when a Task object is created.
    private String description;
    private boolean isDone;

    // The class has a constructor with one parameter, description. This
    // constructor is used to create a new Task object and initialize its
    // description attribute. The isDone attribute is initially set to
    // false for all tasks.
    public Task(String description) {
        this.description = description;
        this.isDone = false;
    }

    // Getter Methods: Two getter methods are provided to access the attributes of a Task object:
    // getDescription(): This method returns the description attribute.
    // isDone(): This method returns the value of the isDone attribute.
    // In contrast, getter methods (like getDescription() and isDone()) typically do not
    // modify the object's state; they just provide access to the object's attributes.
    // Getter methods are used to retrieve information about the object but don't change the
    // object's state, which is why they are not considered behavior methods.
    public String getDescription() {
        return description;
    }

    public boolean isDone() {
        return isDone;
    }

    // The class has a behavior method, markAsDone(), which is used to
    // change the isDone attribute to true, marking the task as done.
    // So, you can identify behavior methods by looking for methods that
    // perform actions or change the state of objects. They are a key part
    // of an object's behavior in an object-oriented program.
    public void markAsDone() {
        isDone = true;
    }

    // @Override: This annotation indicates that you are providing your
    // own implementation of the toString method, which is inherited from
    // the Object class. This is a good practice because it makes your intent
    // clear and helps the compiler catch issues if you accidentally make
    // a mistake in the method signature.
    @Override
    // This code is implementing the toString method from the Object
    // class, which is the root class for all Java classes. The toString
    // method is intended to return a string representation of the object.

    // public String toString(): This line defines the toString
    // method. It's a public method that returns a String.
    public String toString() {
        // return (isDone ? "[X]" : "[ ]") + " " + description;: In this line,
        // you're returning a string that represents the state of the object. The
        // isDone field is a boolean indicating whether the task is completed or not.
        // If isDone is true, it returns "[X]"; otherwise, it returns "[ ]". It then
        // appends a space and the description of the task.
        return (isDone ? "[X]" : "[ ]") + " " + description;
    }
}
