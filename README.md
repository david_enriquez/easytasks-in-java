# Java To-Do List

Manage your tasks efficiently with this simple Java application. Create, remove, and manage your to-do list effortlessly.

## Getting Started

Follow these steps to run the Java To-Do List on your local machine.

### Prerequisites

Ensure that you have Java installed on your system.

### Installation

1. Clone the GitLab repository to your local machine:

    git clone https://gitlab.com/david_enriquez/easytasks-in-java

2. Navigate to the project directory:

    cd JavaToDoList

3. Compile the Java source files:

    javac TodoList.java

4. Run the program:

    java TodoList

Features

- Add new tasks with descriptions.
- Mark tasks as done when completed.
- Remove tasks from your to-do list.
- Interactive command-line interface.
